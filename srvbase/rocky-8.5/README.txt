Build the Rocky Linux 8.5 Server VM
-----------------------------------

Remove previous build:

  $ vagrant destroy

Build the VM:

  $ vagrant up     

Test the VM:

  $ vagrant reload
  $ vagrant ssh
  $ exit
  $ vagrant halt

Build the package:

  $ vagrant package --output rocky-8.5.box

Test the package:

  $ vagrant box add --name local-rocky rocky-8.5.box

