#!/bin/bash
#
# bootstrap.sh

VBOX="download.virtualbox.org/virtualbox"
BOXV="6.1.28"

# Keyboard layout
echo "Configuring console keyboard layout: swiss french"
localectl set-keymap fr_CH-latin1
sleep 1

# Keep en_US.UTF-8 system locale
echo "Keeping en_US.UTF-8 system locale."
sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
systemctl reload sshd 
sleep 1

# Git
echo "Installing Git."
dnf install -y git > /dev/null

# Post-install configuration script
echo "Fetching post-install configuration script."
git clone https://gitlab.com/kikinovak/rocky-el8.git > /dev/null
cd rocky-el8
./linux-setup.sh --setup

# Guest Additions
echo "Downloading VirtualBox Guest Additions."
wget -c http://${VBOX}/${BOXV}/VBoxGuestAdditions_${BOXV}.iso > /dev/null 2>&1
echo "Installing VirtualBox Guest Additions."
mount -o loop VBoxGuestAdditions_${BOXV}.iso /mnt > /dev/null 2>&1
cd /mnt
yes | ./VBoxLinuxAdditions.run > /dev/null 2>&1

# Clean up
dnf clean all

exit 0



