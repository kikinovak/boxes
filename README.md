# Custom boxes for Vagrant Cloud

This repository provides the sources (Vagrantfiles and bootstrap scripts) for
a series of Linux boxes:

## Generic

  * TODO

## Server

  * CentOS 7.8: enhanced base system suitable for servers

## Desktop

  * OpenSUSE Leap 15.1: minimal KDE desktop

  * OpenSUSE Leap 15.2: minimal KDE desktop

Images can be downloaded at Hashicorp's Vagrant Cloud:

  * https://app.vagrantup.com/microlinux/
