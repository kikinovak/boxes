#!/bin/bash
#
# bootstrap.sh

# Keyboard layout
echo "Configuring console keyboard layout: swiss french"
localectl set-keymap fr_CH-latin1
sleep 1

# Keep en_US.UTF-8 system locale
echo "Keeping en_US.UTF-8 system locale."
sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
systemctl reload sshd 
sleep 1

# Vim
echo "Updating package repository information."
zypper refresh > /dev/null
echo "Installing Vim."
zypper install -y vim > /dev/null

exit 0
