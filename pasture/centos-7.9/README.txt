Build the CentOS 7.9 VM
-----------------------

Remove previous build:

  $ vagrant destroy

Build the VM:

  $ vagrant up     

Test the VM:

  $ vagrant reload
  $ vagrant ssh
  $ exit
  $ vagrant halt

Build the package:

  $ vagrant package --output centos-7.9.box

