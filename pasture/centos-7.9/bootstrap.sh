#!/bin/bash
#
# bootstrap.sh
echo "Installing Git."
yum install -y git > /dev/null
echo "Downloading configuration scripts."
git clone https://gitlab.com/kikinovak/centos.git > /dev/null 2>&1
cd centos
./linux-setup.sh --setup
yum clean all
