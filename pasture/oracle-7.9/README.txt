Build the Oracle Linux 7.9 VM
-----------------------------

Remove previous build:

  $ vagrant destroy

Build the VM:

  $ vagrant up     

Test the VM:

  $ vagrant reload
  $ vagrant ssh
  $ exit
  $ vagrant halt

Build the package:

  $ vagrant package --output oracle-7.9.box

