#!/bin/bash
#
# build.sh

BOX="debian-11-minimal"
vagrant halt
rm -f ${BOX}.box
vagrant package --output ${BOX}.box
sha256sum ${BOX}.box > ${BOX}.sha256sum

