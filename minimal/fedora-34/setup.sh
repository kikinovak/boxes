#!/bin/bash
#
# setup.sh

# Variables
VBOX="download.virtualbox.org/virtualbox"
BOXV="6.1.28"
LOGS="/var/log/setup.log"

# Logging
echo "Provisioning logs are written to ${LOGS}."
touch ${LOGS}

# Keyboard layout
echo "Configuring console keyboard layout: fr-ch"
localectl set-keymap ch-fr
sleep 1

# Reinstall Guest Additions properly
echo "Downloading VirtualBox ${BOXV} Guest Additions."
wget -c http://${VBOX}/${BOXV}/VBoxGuestAdditions_${BOXV}.iso >> ${LOGS} 2>&1
echo "Installing required build tools."
dnf install -y bzip2 >> ${LOGS} 2>&1
echo "Installing VirtualBox ${BOXV} Guest Additions."
mount -o loop VBoxGuestAdditions_${BOXV}.iso /mnt >> ${LOGS} 2>&1
cd /mnt
yes | ./VBoxLinuxAdditions.run >> ${LOGS} 2>&1
cd - >> ${LOGS} 2>&1

# Clear login console
echo "Don't flood the console with kernel messages."
echo "kernel.printk = 4 4 1 7" >> /etc/sysctl.conf

# Cleanup
echo "Removing useless files."
rm -f VBoxGuestAdditions_${BOXV}.iso
dnf clean all

# Reboot
echo "Rebooting VM."
reboot

exit 0
