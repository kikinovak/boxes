#!/bin/bash
#
# build.sh

BOX="ubuntu-20.04-minimal"
vagrant halt
rm -f ${BOX}.box
vagrant package --output ${BOX}.box
sha256sum ${BOX}.box > ${BOX}.sha256sum

